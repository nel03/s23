// console.log ('Hi!')

/*
	Objects
		> an object is a data type that is used to represent real world objects. it is also a collection of related data and/or functionalities

	creating object using object literal:

	Syntax:
		let objectName = {
			keyA: valueA,
			keyB: valueB,
		}
*/

	let student ={
		firstName: 'Gin',
		lastName: 'Emperor',
		age: 21,
		studentId: '2022-0031',
		email:['indian@hotmail.com', 'theindian@gmail.com'],
		address:{
			street: '275 E.Rodriguez',
			city: 'Quezon City',
			country: 'The Bar'
		}

	}

	console.log('Result from creating an object:');
	console.log(student);
	console.log(typeof student);

/*
	Creating Objects using Constructor Function

		> create a reusable function to create serveral objects that have the same data structure. this is useful for creating multiple instances/copies of an object

		Syntax:
			function objectName (valueA, ValueB){
				this.keyA = valueA;
				this.keyB = ValueB;
			}

			let variable = new function objectName(valueA, valueB)

			console.log(variable)

			this
				> is a keyword that is used for invoking; it refers to the global object
				> always add "new" keyword when creating a variable
*/

function Laptop(name, manuFactureDate){
	this.name = name;
	this.manuFactureDate = manuFactureDate;
}

let laptop = new Laptop('Lenovo', 2008)
	console.log('result of creating object using object constructor:')
	console.log(laptop);

let myLaptop = new Laptop('Asus', [2020, 2022])
	console.log(myLaptop);

let oldLaptop = Laptop('Portal R2E CCMC', 1980) // new was not included before the Laptop
	console.log(oldLaptop);

//creating empty object as placeholder
let computer = {}
let myComputer = new Object()
console.log(computer);
console.log(myComputer);

myComputer = {
	name: 'Gin',
	manuFactureDate: 2012
}

console.log(myComputer);

//mini activity

function drinks(name, price, size){
	this.name = name;
	this.price = price;
	this.size = size;
}
//at this end its called constructor function


let drink1 = new drinks('Gin', 50, 'sm')
let drink2 = new drinks ('empe', 45, 'md')

console.log(drink1);
console.log(drink2);

/*
	accessing object property

	using the dot notation

	styntax:
		objectName.propertyName
*/

console.log('result from dot notation ' + myLaptop.name)

/*
	using the bracket notation
	syntax:
		objectName['name']
*/

console.log('result from bracket notation ' + myLaptop.name)

//Accessing array object

let array = [laptop, myLaptop]

// let array = [{name: 'Lenovo',manuFacturingDate: 2008}, {name: 'Asus', manuFactureDate: [2020, 2022]}]

//dot notation

console.log(array[0].name); // use to call the content

//square bracket notation
console.log(array[0]['name']);


/*
	initalizin/ adding/ deleting/ reassigning object properties
*/

let car = {} // empty container
console.log(car)

// add object in the container

car.name = 'Honda Civic'
console.log('result from adding property using dot notation')
console.log(car);

car['manuFactureDate'] = 2019;
console.log(car);

car.name = ['Ferrari', 'Toyota']
console.log(car)

//deleting object properties

// delete car['manuFactureDate']
// car['manuFactureDate'] = ' '

console.log('result from deleting object properties')
console.log(car);

// reassigning object properties

car.name = 'Tesla'
console.log('result from reassigning object properties')
console.log(car)


/*
	Object Method
		>a method where a function serves as a value in a property. they are also funtions and one of the key differences they have is the methods are function related to a specific object property
*/

let person = {
	name: 'john',
	talk:function(){
		console.log('hi my name is ' + this.name);
	}
}

console.log(person)
console.log('result from object methods:')
person.talk()

person.walk = function(){
	console.log(this.name + "walked 25 steps forward")
}

person.walk()

let friend = {
	firstName: 'john',
	lastName: 'doe',
	address: {
			city: "austin, texas",
			country: 'US'
	},
	emails: ['idap@gmail.com', 'wait@yahoo.com'],
	introduce: function(){
		console.log("hello! my name is " + this.firstName + " " + this.lastName)
	}
}

friend.introduce()

//Real World Application

/*
	scenario
		1. we would like to creat a game that would have serveral pokemon to interact with each other.
		2. every pokemon would have the same sets of stats, properties and functions.
*/


// using object literals

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon's")
		console.log("TargetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}

}

console.log (myPokemon)

// using Object constructor

function Pokemon(name, level){
	
//properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = 2 * level;

//methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log(target.name + "'s health is now reduced " + (target.health - this.attack));
		if(target.health - this.attack <=5){
			target.faint();
		}
		target.health = target.health - this.attack;
		console.log(target);
	},
	this.faint = function(){
		console.log(this.name + " fainted")
	}
}

let charizard = new Pokemon("Charizard", 12)
let squirtle = new Pokemon ("Squirtle", 6)

console.log(charizard)
console.log(squirtle)

charizard.tackle(squirtle)


//MAIN ACTIVITY:
/*
WDC028v1.5b-23 | JavaScript - Objects
Graded Activity:

1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
	- Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
	(target.health - this.attack)

2.) If health is less than or equal to 5, invoke faint function

*/
//Code Here:


let Trainer = {
	name: 'Ash',
	age: 3,
	pokemon: {
		pokemon1: 'Pikachu',
		pokemon2: 'Beedrill',
		pokemon3: 'Blastoise',
		pokemon4: 'Ninetales'
	},
	friends: ['Brawly', 'Wattson', 'Flannery'],
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
	
}


console.log('Result of dot notation:')
console.log(Trainer.name)

console.log('Result of square bracket notation:')
console.log(Trainer['pokemon'])

console.log('Result of talk method')
Trainer.talk()


// 2



let pikachu = new Pokemon("Pikachu", 12)
let ninetales = new Pokemon ("Ninetales", 4)
let blastoise = new Pokemon ("Blastoise", 4)

console.log(pikachu);
console.log(ninetales);
console.log(blastoise);

ninetales.tackle(pikachu)
blastoise.tackle(ninetales)



